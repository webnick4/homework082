const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');

mongoose.connect(config.db.url +  '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropCollection('artists');
    await db.dropCollection('albums');
    await db.dropCollection('tracks');
  } catch (err) {
    console.log('Something was not deleted');
  }

  const [oneArtist, twoArtist] = await Artist.create({
    name: 'Michael Jackson',
    photo: 'michael-jackson.jpeg',
    description: 'Американский певец, автор песен, музыкальный продюсер, аранжировщик, танцор, хореограф, актёр, сценарист, филантроп, предприниматель.'
  }, {
    name: 'Sting',
    photo: 'sting.jpeg',
    description: 'Британский музыкант, певец и автор песен, мульти-инструменталист, активист, актёр и филантроп.'
  });

  const [oneAlbum, twoAlbum] = await Album.create({
    title: 'Dangerous',
    artist_name: 'Michael Jackson',
    year: '1991',
    cover: 'dangerous.jpeg',
    artist: oneArtist._id
  }, {
    title: 'Fields Of Gold - The Best Of Sting 1984 - 1994',
    artist_name: 'Sting',
    year: '1994',
    cover: 'fields-of-gold.jpeg',
    artist: twoArtist._id
  });

  const [oneTrack, twoTrack] = await Track.create({
    song: 'Remember the Time',
    album_title: 'Dangerous',
    track_time: '3:59',
    album: oneAlbum._id
  }, {
    song: 'Fields Of Gold',
    album_title: 'Fields Of Gold - The Best Of Sting 1984 - 1994',
    track_time: '3:38',
    album: twoAlbum._id
  });

  db.close();
});