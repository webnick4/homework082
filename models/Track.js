const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TrackSchema = new Schema({
  song: {
    type: String,
    required: true
  },
  album_title: {
    type: String,
    required: true
  },
  track_time: {
    type: String,
    required: true
  },
  album: {
    type: Schema.Types.ObjectId,
    ref: 'Album',
    required: true
  }
});

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;