const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AlbumSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  artist_name: {
    type: String,
    required: true
  },
  year: {
    type: String,
    required: true
  },
  cover: String,
  artist: {
    type: Schema.Types.ObjectId,
    ref: 'Artist',
    required: true
  }
});

const Album = mongoose.model('Album', AlbumSchema);

module.exports = Album;