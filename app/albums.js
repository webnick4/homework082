const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const Album = require('../models/Album');
const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPathAlbum);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const createRouter = () => {
  const router = express.Router();

  router.get('/', (req, res) => {
    const artist_id = req.param('artist');

    if (artist_id) {
      Album.find().where('artist').equals(artist_id)
        .then(result => {
          if (result) res.send(result);
          else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));

    } else {
      Album.find()
        .then(albums => res.send(albums))
        .catch(() => res.sendStatus(500));
    }
  });

  router.post('/', upload.single('cover'), (req, res) => {
    const albumData = req.body;

    if (req.file) {
      albumData.cover = req.file.filename;
    } else {
      albumData.cover = null;
    }

    const album = new Album(albumData);

    album.save()
      .then(album => res.send(album))
      .catch(error => res.status(400).send(error));
  });

  return router;
};

module.exports = createRouter;