const express = require('express');

const Track = require('../models/Track');

const createRouter = () => {
  const router = express.Router();

  router.get('/', (req, res) => {
    const album_id = req.param('album');

    if (album_id) {
      Track.find().where('album').equals(album_id)
        .then(result => {
          if (result) res.send(result);
          else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));

    } else {
      Track.find()
        .then(tracks => res.send(tracks))
        .catch(() => res.sendStatus(500));
    }
  });

  router.post('/', (req, res) => {
    const track = new Track(req.body);

    track.save()
      .then(track => res.send(track))
      .catch(error => res.status(400).send(error));
  });

  return router;
};

module.exports = createRouter;