const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');

const createRouter = () => {
  const router = express.Router();

  router.post('/', async (req, res) => {
    const getToken = req.get('Token');

    const user = await User.findOne({token: getToken});

    if (getToken !== user.token) return res.status(401).send({error: 'Unauthorized!'});

    const track_history = req.body;
    track_history.user = user._id;

    const trackHistory = new TrackHistory(track_history);

    trackHistory.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));

  });

  return router;
};

module.exports = createRouter;