const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  uploadPathAlbum: path.join(rootPath, 'public/uploads/albums'),
  db: {
    url: 'mongodb://localhost:27017',
    name: 'music-app'
  }
};